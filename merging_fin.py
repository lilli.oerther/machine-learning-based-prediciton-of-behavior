# -*- coding: utf-8 -*-
"""
Created on Thu Aug 31 16:11:38 2023

@author: lilli
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Aug  5 13:24:08 2023

@author: lilli
"""

import pandas as pd
import numpy as np
from glob import glob
import re
import time

# load needed files 

def load_participant (x):

    path=pd.DataFrame(glob("C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Hallway Illusion/data/behavior_data/20230816/"+str(x)+"/*"))
    path.columns=['location']
    
    d={}
    for i in range (len(path['location'])):
        da=pd.read_csv(str(path['location'].iloc[i]))
        
        p=time.strptime(str(da['date'][0]), '%Y_%b_%d_%H%M' )
        dr=[]
        for r in range (len(da)):
            dr.append(np.cumsum(da['touch_delay']))
        da=da.join(pd.DataFrame({'frames_total': dr}))
        
        d[str(x)+'_0'+str(p[1])+str(p[2])+'_'+str(p[3])+str(p[4])]=da
    keys = [k for k, v in d.items()]
    return d, keys

def merging (mouse_id, r):

    #load green light sequence
    path=pd.DataFrame(glob("C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Hallway Illusion/data/filtered_videos/*"))
    path.columns=['location']
    x_id= str((path.iloc[r])[0])
    
    seq= pd.read_csv(str(x_id))
    seq= seq.columns
    seq= seq[1:-1].astype('float')
    #fps= seq[1].astype('flaot)
    #seq= seq[2:-1].astype('float')
    
    # get time difference between onsets of sequence
    diff=[]
    for r in range (len(seq)-1):
        diff.append(seq[r+1]-seq[r])    

    data_behavior, key = load_participant(mouse_id) #load behavior data
    
    #define variable from behavior data
    touch=data_behavior[key[0]]
    touch_delay=touch['touch_delay']
    lick_delay=touch['lick_delay']
    correct= touch["correct"]
    end_trial= np.cumsum(lick_delay)
    
    # get beginning frame of each trial
    begin_trial=[]
    for r in range (len(end_trial)):
        a = touch_delay[r]
        if r==0:
            b=0
        else:  
            b = end_trial[r-1]
        begin_trial.append(a+b)
        
    # get all time points with correct trial from behavior data
    touch_tpoint=[]
    for r in range (len(correct)):
        if correct[r]==1:
            touch_tpoint.append(begin_trial[r])
    
    # calculate difference between two timepoints
    behavior_diff=[]
    for r in range (len(touch_tpoint)-1):
        behavior_diff.append(touch_tpoint[r+1]-touch_tpoint[r])
    
            
    # compare: time point differences from sequence with time point differences from behavior data       
    behavior_seq=[]
    if len(diff)<10:
        l=len(diff)
    else: 
        l=10
    
    def compare_loop (x):
        empty=[]
        behavior_seq=[]
        for r in range (len(behavior_diff) - l-x):
            for p in range(l):
                if behavior_diff[r+p+x] > diff[p+x]-1 and behavior_diff[r+p+x] < diff[p+x]+1:
                    empty.append(1)
                else:
                    empty.append(0)
            n=np.cumsum(empty)
            if n[-1] == l:
                behavior_seq.append(touch_tpoint[(r+x):r+l+x])
            empty=[]
        return behavior_seq, x
    
    for x in range(len(diff)): 
        if len(behavior_seq)==0:
            behavior_seq, i= compare_loop(x)
            
    #time point difference between behavior data and video 
    t_point= seq[i]- behavior_seq[0][0]
    start_points={"t_video[s]": (0.0), "t_script[s]": (t_point)}
    #c= [0, fps, t_point, 0]
    c= [0,t_point,0]
    #save t_points as csv data
    mouse_id= re.split("filtered_videos|.csv", x_id)[1] #get mouse_id + date as title
    #save comparison/ lamp onsets as csv 
    file = open(r"C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Hallway Illusion/data/merged_behavior_tracked"+str(mouse_id)+'.csv','w') 
    file.writelines(str(c))
    file.close()

    return start_points



start_points = merging('2694', 5)



