# -*- coding: utf-8 -*-
"""
Created on Sun Sep 17 15:53:41 2023

@author: lilli
"""

import pandas as pd
import numpy as np
import matplotlib.pylab as plt

import matplotlib as mpl
import seaborn as sns
from scipy.spatial import distance
from glob import glob
import re

#game 6-10
# green right
id_ = 10 
team_black = 'Team 2'          
team_green = 'Team 5'       
t_between_goals = 1.431395944701928613e+01 
fps = 240
dframe = fps * t_between_goals
lp = 0.9

# function to get float values from goal cvs 
def get_goals_float (goals):
    g = goals.columns
    if len(g) > 1:
        
        g_1= [float(g[0].rsplit('[',1)[-1])]
        g_2 = [float(g[-1].rsplit(']',1)[0])]
        goals= g[1:-1].astype('float').tolist()
        goals = np.concatenate((g_1, goals, g_2))
    else:
        g = g[0].rsplit('[',1)[1]
        g = g.rsplit(']',1)[0]
        if g != '':
            goals = [float(g)]
        else:
            goals = []
    return goals

# load goals
def load_goals (id_):    
    
    path = glob(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\goals\240_fps\practical_course\game_"+str(id_)+"\*")
    path_2 = glob(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\DLC_tracking_defense\practical_course\game_"+str(id_)+"\*")
    
    
    goals_black = []
    goals_green = []
    game = []
    goals_total = []
    x = []
    y = []
    like = []
    
    for r in range (len(path_2)):
        game_id = re.split('team_', path[r*2])[0]
        
        goals_bl = pd.read_csv(str(game_id)+"team_black.csv")
        goals_gr = pd.read_csv(str(game_id)+"team_green.csv")
        
        goals_bl = get_goals_float (goals_bl)
        goals_gr = get_goals_float(goals_gr)
        goals_t = np.sort(np.concatenate((goals_bl, goals_gr)))
        
        # load tracking data
        data= pd.read_csv(path_2[r])
        data.loc[2:]=data.loc[2:].astype(float)
        
        x_d = data.iloc[2:,25]
        y_d = data.iloc[2:, 26]
        like_d = data.iloc[2:,27]
        
        goals_total.append(goals_t)
        goals_black.append(goals_bl)
        goals_green.append(goals_gr)
       
        game.append(data)
        x.append(x_d)
        y.append(y_d)
        like.append(like_d)
    
    return goals_black, goals_green, goals_total, game, x, y, like

goals_black, goals_green, goals_total, game, x, y, like = load_goals(id_)


# get 17 sec before goal and sort 
def get_trials (goal_all, x_all, y_all, like_all, t):
    
    correct_x =[]
    correct_y = []
    
    for h in range (len(x_all)):
        goal_d = goal_all[h]
        x = x_all[h]
        y = y_all[h]
        like = like_all[h]
        
        for r in range (len(goal_d)):
            i = int(goal_d[r])
            b = int(goal_d[r]-dframe)
            c = int(goal_d[r]+dframe)
            if b > 0 and t>0:
                correct_x.append(x.iloc[b:i][like>lp])
                correct_y.append(y.iloc[b:i][like>lp])
            if c < len(x) and t ==0:
                    correct_x.append(x.iloc[i:c][like>lp])
                    correct_y.append(y.iloc[i:c][like>lp])
                    
    return correct_x, correct_y

correct_x, correct_y = get_trials(goals_total, x, y, like, 1)
aftergoal_x, aftergoal_y = get_trials(goals_total, x, y, like, 0)
green_x, green_y = get_trials (goals_green, x, y, like, 1)
black_x, black_y = get_trials (goals_black, x, y, like, 1)


#plotting
#trajectory

def plotting(data_x, data_y, ax1, ax2, label):
    for r in range(len(data_x)):
        x = data_x[r]
        y = data_y[r]
        
        #axes[ax1,ax2].scatter(x, y, label=label, color='black', s = 0.1)
        axes[ax1,ax2].plot(x, y, linewidth=0.3, label=label, color='black')
        axes[ax1, ax2].set_xlim([0,2000])
        axes[ax1, ax2].set_ylim([0,1200])
        axes[ax1,ax2].set_title(label)
        
        if len(data_x) ==0 :
            message = 'No Goals'
            axes[ax1, ax2].text(6.0, 500, message)
        
        if ax1 == 1:
            axes[ax1,ax2].set_xlabel('X-coordinates')
        if ax2 == 0:
            axes[ax1,ax2].set_ylabel('Y-coordinates')
    return 

plt.figure()
fig, axes = plt.subplots(2,2, sharey=True, sharex=True, figsize=(9,6))
fig.tight_layout(h_pad=2)
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font
plotting(correct_x, correct_y, 0, 0, ' Before Goals')
plotting(green_x, green_y, 1,0, ('Before Goals '+str(team_green)))
plotting(black_x, black_y, 1,1, ('Before Goals '+str(team_black)))
plotting(aftergoal_x, aftergoal_y, 0,1, 'After Goals')
axes[0,0].text(-0.2, 1.05, 'A', font={'size':16} ,transform=axes[0,0].transAxes)

sns.despine()

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc\trajectories/"+str(id_)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc\trajectories/"+str(id_)+".svg", dpi=300,  bbox_inches='tight')

def plot_hist (data_x, data_y, ax1, ax2, label):
    
    cx = []
    cy = []
    
    for r in range(len(data_x)):
        x = data_x[r]
        y = data_y[r]
        for k in range (len(x)):
            cx.append(x.iloc[k])
            cy.append(y.iloc[k])
        
    
    axes[ax1, ax2].hist2d(cx,cy, bins=[20, 20], range=np.array([(0, 2000), (0, 1200)]), cmap='YlGnBu_r')
    axes[ax1,ax2].set_title(label)
    
    if ax1 == 1:
        axes[ax1,ax2].set_xlabel('X-Coordinates')
    if ax2 == 0:
        axes[ax1,ax2].set_ylabel('Y-Coordinates')
    return cx, cy

plt.figure()
fig, axes = plt.subplots(2,2, sharey=True, sharex=True, figsize=(12, 6))
fig.tight_layout(h_pad=2)
plt.subplots_adjust(wspace=0.15) # distance between plots, width

# parameters legend
N = 21
norm = mpl.colors.Normalize(vmin=0,vmax=2)
sm = plt.cm.ScalarMappable( norm=norm, cmap = 'YlGnBu_r')
sm.set_array([])
fig.colorbar(sm, ticks=np.linspace(0,4,N), 
             boundaries=np.arange(0,2.1,.1), ax = axes[:,:])

font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font
cx, cy = plot_hist(correct_x, correct_y, 0,0, 'Before Goals')
gx, gy = plot_hist(green_x, green_y,1,0, ('Before Goals '+str(team_green)))
bx, by = plot_hist(black_x, black_y, 1,1, ('Before Goals '+str(team_black)))
ax, ay = plot_hist(aftergoal_x, aftergoal_y, 0,1, 'After Goals')

axes[0,0].text(-0.2, 1.05, 'B', font={'size':16} ,transform=axes[0,0].transAxes)

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc\heatmap/"+str(id_)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc\heatmap/"+str(id_)+".svg", dpi=300,  bbox_inches='tight')


#velocity
def get_velocity (data_x, data_y):
#data_x = correct_x
#data_y = correct_y
    
    dt_0= 1/fps
    i = dframe/fps
    vel = [] # for every goal: mean velcoity of every second: nr_goals * 17 * mean_vel
    
    for m in range(len(data_x)):
        v = []
        if len(data_x[m]) > i:

            for s in range (int(i)):
                l = (1/i) * len(data_x[m])
                i1 = int(s*l)
                i2 = int(i1 + l)
                cx_p = (data_x[m][i1:i2]) # x_values sorted into seconds : l * x_values
                cy_p = (data_y[m][i1:i2]) # y_values sorted into seconds : l * y_values
                
                vv = []
                for k in range(len(cx_p)-1):
                    a = cx_p.iloc[k], cy_p.iloc[k]
                    b = cx_p.iloc[k+1], cy_p.iloc[k+1]
                    d = distance.euclidean(a,b) # get euclidean distance of every tuple
                    vv.append(d/dt_0) # calculate and append velocity
                    
                v.append(np.mean(vv)) # calculate and append mean velocity of every second
        vel.append(v) # append mean_velocities
    
    # calculate mean velocity of every second over all goals
    mean_speed = []
    t = []
    for z in range(int(i)):
        e  = []
        for y in range(len(vel)):
            e.append(vel[y][z])
        mean_speed.append(np.mean(e)) #append mean velocity in mean_speed 2times
        mean_speed.append(np.mean(e))
        t.append(z)
        t.append(z+1)
            
    return mean_speed, t, vel

def plot_vel (dx, dy, ax1, ax2, label):
     
    goals_vel, t, vel = get_velocity(dx, dy)
    
    
    axes[ax1, ax2].plot(t, goals_vel, color = 'black')
    axes[ax1,ax2].set_title(label)
    
    if len(vel) ==0 :
        message = 'No Goals'
        axes[ax1, ax2].text(7.0, 750, message)
    # using rc function
    if ax1 == 1:
       # mpl.rc('font', **font_a)
        axes[ax1,ax2].set_xlabel('Time [s]')
    if ax2 == 0:
       # mpl.rc('font', **font_a)
        axes[ax1,ax2].set_ylabel('Velocity [px/s]')
        
    return goals_vel, vel
    


plt.figure()
fig, axes = plt.subplots(2,2, sharey=True, sharex=True, figsize=(9, 6))
fig.tight_layout(h_pad=2)
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font          

all_goals_vel, o = plot_vel(correct_x, correct_y, 0,0, 'Before Goals')
green_goals_vel, k = plot_vel(green_x, green_y, 1,0, ('Before Goals '+str(team_green)))
black_goals_vel, l = plot_vel(black_x, black_y, 1,1, ('Before Goals '+str(team_black)))
after_goals_vel, mm = plot_vel(aftergoal_x, aftergoal_y, 0,1, 'After Goals')
axes[0,0].text(-0.2, 1.05, 'C', font={'size':16} ,transform=axes[0,0].transAxes)

sns.despine()

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc\velocity/"+str(id_)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc\velocity/"+str(id_)+".svg", dpi=300,  bbox_inches='tight')

# histogramm

# create dataframe with all correct goals and sort based on team    
team = [] # team list for dataframe
green_ind =[]
black_ind=[] 
for r in range(len(green_x)):
    s = green_x[r].index[:]
    green_ind = np.concatenate((s, green_ind))
for r in range(len(black_x)):
    s = black_x[r].index[:]
    black_ind = np.concatenate((s, black_ind))

ind =[]
for r in range (len(bx)):
    team.append(str(team_black))
    ind.append(black_ind[r])
for r in range (len(gx)):
    team.append(str(team_green))
    ind.append(green_ind[r])

cc_x = np.concatenate((bx, gx)) # concatenate green and black x values
cc_y = np.concatenate((by, gy)) # concatenate green and black y values

# create dataframe from arrays 
df = pd.DataFrame(data=[ind, cc_x, cc_y, team], index=['ind','x', 'y', 'Team'])
df = df.transpose() # switch index with columns
df = df.sort_values(by=['ind']) # sort after column = 'ind'


plt.figure()
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font 
im = sns.jointplot(ax = ax[0], data=df, x="x", y="y", color = 'black', hue='Team', s = 2.5)
im.fig.suptitle('Before Goals Game '+str(id_))
im.fig.subplots_adjust(top=0.925) 
plt.legend(bbox_to_anchor = [1.6,0.2])
#plt.text(-0.1, 0.95, 'D', font={'size':16} ,transform=axes[0,0].transAxes)


#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc/legend"+str(id_)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc/legend"+str(id_)+".svg", dpi=300,  bbox_inches='tight')


# alternative

df = pd.DataFrame(data=[cx, cy], index=['x', 'y'])
df = df.transpose()

plt.figure()
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font 
im = sns.jointplot(ax = ax[0], data=df, x="x", y="y", color = 'black', s = 2.5)
im.fig.suptitle('Before Goals')
im.fig.subplots_adjust(top=0.925) 
plt.text(-0.1, 0.95, 'D', font={'size':16} ,transform=axes[0,0].transAxes)

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc/"+str(id_)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\with_after_goals\like_filter_90perc/"+str(id_)+".svg", dpi=300,  bbox_inches='tight')

