# -*- coding: utf-8 -*-
"""
Created on Sun Sep 17 15:53:41 2023

@author: lilli
"""

import pandas as pd
import numpy as np
import matplotlib.pylab as plt

import matplotlib as mpl
import seaborn as sns
from scipy.spatial import distance

id_ = 11
team_black = 'Team Black'
team_green = 'Team Green' 
t_between_goals = 1.759421835255280087e+01
fps = 24
dframe = fps * t_between_goals

# function to get float values from goal cvs 
def get_goals_float (goals):
    g = goals.columns
    if len(g) > 1:
        
        g_1= [float(g[0].rsplit('[',1)[-1])]
        g_2 = [float(g[-1].rsplit(']',1)[0])]
        goals= g[1:-1].astype('float').tolist()
        goals = np.concatenate((g_1, goals, g_2))
    else:
        g = g[0].rsplit('[',1)[1]
        g = g.rsplit(']',1)[0]
        if g != '':
            goals = [float(g)]
        else:
            goals = []
    return goals

# load goals
def load_goals (id_):    
    
    game = pd.read_csv(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\pickle_tournament1\g"+str(id_)+"DLC_pickle.csv")
    game.loc[2:]=game.loc[2:].astype(float)
    
    goals_bl = pd.read_csv(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\goals\24_fps\g"+str(id_)+"_black.csv")
    goals_gr = pd.read_csv(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\goals\24_fps\g"+str(id_)+"_green.csv")
    
    goals_black = get_goals_float(goals_bl)
    goals_green = get_goals_float(goals_gr)
    goals_total = np.sort(np.concatenate((goals_black, goals_green)))
        
    x = game.iloc[2:,17]
    y = game.iloc[2:, 18]
    
    
    return goals_black, goals_green, goals_total, game, x, y

goals_black, goals_green, goals_total, game, x, y = load_goals(id_)


# get 17 sec before goal and sort 
def get_trials (goal_all, x_all, y_all, t):
    
    correct_x =[]
    correct_y = []
    
    goal_d = goal_all
        
    for r in range (len(goal_d)):
        i = int(goal_d[r])
        b = int(goal_d[r]-dframe)
        c = int(goal_d[r]+dframe)
        if b > 0 and t>0:
            correct_x.append(x.iloc[b:i][np.isnan(x) == False])
            correct_y.append(y.iloc[b:i][np.isnan(x) == False])
        if c < len(x) and t ==0:
                correct_x.append(x.iloc[i:c][np.isnan(x) == False])
                correct_y.append(y.iloc[i:c][np.isnan(x) == False])
                    
    return correct_x, correct_y

correct_x, correct_y = get_trials(goals_total, x, y, 1)
aftergoal_x, aftergoal_y = get_trials(goals_total, x, y, 0)
green_x, green_y = get_trials (goals_green, x, y, 1)
black_x, black_y = get_trials (goals_black, x, y, 1)


#plotting
#trajectory

def plotting(data_x, data_y, ax1, ax2, label):
    for r in range(len(data_x)):
        x = data_x[r]
        y = data_y[r]
        
        axes[ax1,ax2].plot(x, y, linewidth=0.3, label=label, color='black')
        axes[ax1, ax2].set_xlim([200,2000])
        axes[ax1, ax2].set_ylim([0,1100])
        axes[ax1,ax2].set_title(label)
        
        if len(data_x) ==0 :
            message = 'No Goals'
            axes[ax1, ax2].text(6.0, 500, message)
        
        if ax1 == 1:
            axes[ax1,ax2].set_xlabel('X-coordinates')
        if ax2 == 0:
            axes[ax1,ax2].set_ylabel('Y-coordinates')
    return 

plt.figure()
fig, axes = plt.subplots(2,2, sharey=True, sharex=True, figsize=(9,6))
fig.tight_layout(h_pad=2)
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font
plotting(correct_x, correct_y, 0, 0, ' Before All Goals')
plotting(green_x, green_y, 1,0, ('Before Goals '+str(team_green)))
plotting(black_x, black_y, 1,1, ('Before Goals '+str(team_black)))
plotting(aftergoal_x, aftergoal_y, 0,1, 'After All Goals')
fig.suptitle('Multi-animal', y = 1.05, fontsize = 18)
axes[0,0].text(-0.2, 1.20, 'A', font={'size':16} ,transform=axes[0,0].transAxes)

sns.despine()

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\pickle/lineplot_"+str(id_)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\pickle/lineplot_"+str(id_)+".svg", dpi=300,  bbox_inches='tight')

def plot_hist (data_x, data_y, ax1, ax2, label):
    
    cx = []
    cy = []
    
    for r in range(len(data_x)):
        x = data_x[r]
        y = data_y[r]
        for k in range (len(x)):
            cx.append(x.iloc[k])
            cy.append(y.iloc[k])
        
    
    axes[ax1, ax2].hist2d(cx,cy, bins=[20, 20], range=np.array([(200, 2000), (0, 1200)]), cmap='YlGnBu_r')
    axes[ax1,ax2].set_title(label)
        
    if ax1 == 1:
        axes[ax1,ax2].set_xlabel('X-Coordinates')
    if ax2 == 0:
        axes[ax1,ax2].set_ylabel('Y-Coordinates')
    return cx, cy

plt.figure()
fig, axes = plt.subplots(2,2, sharey=True, sharex=True, figsize=(12, 6))
fig.tight_layout(h_pad=2)
plt.subplots_adjust(wspace=0.15) # distance between plots, width

# parameters legend
N = 21
norm = mpl.colors.Normalize(vmin=0,vmax=2)
sm = plt.cm.ScalarMappable( norm=norm, cmap = 'YlGnBu_r')
sm.set_array([])
fig.colorbar(sm, ticks=np.linspace(0,4,N), 
             boundaries=np.arange(0,2.1,.1), ax = axes[:,:])

font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font
cx, cy =plot_hist(aftergoal_x, aftergoal_y, 0,0, 'Before Goals')
gx, gy=plot_hist(green_x, green_y,1,0, ('Before Goals '+str(team_green)))
bx, by=plot_hist(black_x, black_y, 1,1, ('Before Goals '+str(team_black)))
ax, ay=plot_hist(aftergoal_x, aftergoal_y, 0,1, 'After Goals')
fig.suptitle('Multi-animal', y = 1.05, fontsize = 18)
axes[0,0].text(-0.2, 1.2, 'B', font={'size':16} ,transform=axes[0,0].transAxes)

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\pickle\heatmap"+str(id_)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\pickle\heatmap"+str(id_)+".svg", dpi=300,  bbox_inches='tight')


#velocity
def get_velocity (data_x, data_y):
#data_x = correct_x
#data_y = correct_y
    
    dt_0= 1/fps
    i = dframe/fps
    vel = [] # for every goal: mean velcoity of every second: nr_goals * 17 * mean_vel
    
    for m in range(len(data_x)):
        v = []
        if len(data_x[m]) > i:

            for s in range (int(i)):
                l = (1/i) * len(data_x[m])
                i1 = int(s*l)
                i2 = int(i1 + l)
                cx_p = (data_x[m][i1:i2]) # x_values sorted into seconds : l * x_values
                cy_p = (data_y[m][i1:i2]) # y_values sorted into seconds : l * y_values
                
                vv = []
                for k in range(len(cx_p)-1):
                    a = cx_p.iloc[k], cy_p.iloc[k]
                    b = cx_p.iloc[k+1], cy_p.iloc[k+1]
                    d = distance.euclidean(a,b) # get euclidean distance of every tuple
                    vv.append(d/dt_0) # calculate and append velocity
                    
                v.append(np.mean(vv)) # calculate and append mean velocity of every second
        vel.append(v) # append mean_velocities
    
    # calculate mean velocity of every second over all goals
    mean_speed = []
    t = []
    for z in range(int(i)):
        e  = []
        for y in range(len(vel)):
            if len(vel[y]) >= int(i):
                e.append(vel[y][z])
        mean_speed.append(np.mean(e)) #append mean velocity in mean_speed 2times
        mean_speed.append(np.mean(e))
        t.append(z)
        t.append(z+1)
            
    return mean_speed, t, vel

def plot_vel (dx, dy, ax1, ax2, label):
     
    goals_vel, t, vel = get_velocity(dx, dy)
    
    
    axes[ax1, ax2].plot(t, goals_vel, color = 'black')
    axes[ax1, ax2].set_ylim([0,1500])
    axes[ax1,ax2].set_title(label)
    
    if len(vel) ==0 :
        message = 'No Goals'
        axes[ax1, ax2].text(7.0, 750, message)
    if ax1 == 1:
        axes[ax1,ax2].set_xlabel('Time [s]')
    if ax2 == 0:
        axes[ax1,ax2].set_ylabel('Velocity [px/s]')
        
    return goals_vel, vel
    


plt.figure()
fig, axes = plt.subplots(2,2, sharey=True, sharex=True, figsize=(9, 6))
fig.tight_layout(h_pad=2)
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font          

all_goals_vel, o = plot_vel(correct_x, correct_y, 0,0, 'Before Goals')
green_goals_vel, k = plot_vel(green_x, green_y, 1,0, ('Before Goals '+str(team_green)))
black_goals_vel, l = plot_vel(black_x, black_y, 1,1, ('Before Goals '+str(team_black)))
after_goals_vel, mm = plot_vel(aftergoal_x, aftergoal_y, 0,1, 'After Goals')
fig.suptitle('Multi-animal', y = 1.05, fontsize = 18)
axes[0,0].text(-0.2, 1.20, 'A', font={'size':16} ,transform=axes[0,0].transAxes)

sns.despine()

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\pickle/velocity"+str(id_)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\pickle\velocity"+str(id_)+".svg", dpi=300,  bbox_inches='tight')


# histogramm

# create dataframe with all correct goals and sort based on team    
team = [] # team list for dataframe
green_ind =[]
black_ind=[] 
for r in range(len(green_x)):
    s = green_x[r].index[:]
    green_ind = np.concatenate((s, green_ind))
for r in range(len(black_x)):
    s = black_x[r].index[:]
    black_ind = np.concatenate((s, black_ind))

ind =[]
for r in range (len(bx)):
    team.append(str(team_black))
    ind.append(black_ind[r])
for r in range (len(gx)):
    team.append(str(team_green))
    ind.append(green_ind[r])

cc_x = np.concatenate((bx, gx)) # concatenate green and black x values
cc_y = np.concatenate((by, gy)) # concatenate green and black y values

# create dataframe from arrays 
df = pd.DataFrame(data=[ind, cc_x, cc_y, team], index=['ind','x', 'y', 'Team'])
df = df.transpose() # switch index with columns
df = df.sort_values(by=['ind']) # sort after column = 'ind'


plt.figure()
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font 
im = sns.jointplot(ax = axes[0], data=df, x="x", y="y", hue='Team', s = 2.5, xlim=[0, 2200], ylim=[-200, 1200])
#im.fig.suptitle('Before Goals '+str(id_))
im.fig.subplots_adjust(top=0.925) 
plt.legend(bbox_to_anchor = [1.7,0.2])
im.fig.suptitle('Multi-animal', y = 1.0, fontsize = 18)
#plt.text(-0.1, 1.0, 'A', font={'size':16} ,transform=axes[0,0].transAxes)

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\pickle/scatter"+str(id_)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\pickle\scatter"+str(id_)+".svg", dpi=300,  bbox_inches='tight')
