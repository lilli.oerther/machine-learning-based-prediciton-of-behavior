# -*- coding: utf-8 -*-
"""
Created on Mon Sep  4 13:14:35 2023

@author: lilli
"""

import pandas as pd

# fuction to load game 
def load_data(game_nr):
    data= pd.read_csv(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\DLC_tracking_defense\24_fps\g"+str(game_nr)+"DLC_resnet50_kicker_defense_bJun1shuffle1_200000.csv")
    data.loc[2:]=data.loc[2:].astype(float)
    
    return data

game= load_data(1)                           

x_left = 1100
x_right = 1100 
fps= 24.00
min_f = int(fps)
n1= int(0.25*fps)
n2= int(fps)

e=[]
#black
goal=[]
for r in range (len(game)-3):
    if game.iloc[2+r,10]< 550 and game.iloc[2+r,12] > 0.5 and game.iloc[3+r,12]< 0.5:
        e.append(r)
        if sum (game.iloc[r+2-n1: r+2, 12]) > n1*0.5 and sum(game.iloc[r+2: r+2+n2, 12]) < n2*0.5:
            goal.append(r+2)
            
if len(goal)>0:         
    g=[goal[0]]          
    a=g[0]
    # between two goals need to be at least 5 seconds
    for r in range (len(goal)-1):
        if (goal[r+1]-a) > fps*5:
            g.append(goal[r+1])
            a= goal[r+1]
else: 
    g=goal
    
#green
goal_g=[]
e_g=[]
for r in range (len(game)-3):
    if game.iloc[2+r,10]> 1675 and game.iloc[2+r,12] > 0.65 and game.iloc[3+r,12]< 0.65:
        e_g.append(r)
        if sum (game.iloc[r+2-n1: r+2, 12]) > n1*0.5 and sum(game.iloc[r+2: r+2+n2, 12]) < n2*0.5:
            goal_g.append(r+2)
            
if len(goal_g)>0:         
    g_g=[goal_g[0]]   
    a=g_g[0]       
    # between two goals need to be at least 5 seconds
    for r in range (len(goal_g)-1):
        if (goal_g[r+1]- a) > fps*5:
            g_g.append(goal_g[r+1])
            a= goal_g[r+1]
else: 
    g_g=goal_g
    
    
# decide which frame:
# get frame when ball visible again (sequence > 0.65)
# get frame closest to center

     
#edit   
def edit (goal, game, game_id):
    if len(goal)>1: 
        ep=[]
        o=[]
        j=[]
        
        for l in range (len(goal)):
            d = goal[l]
            if l == len(goal)-1:
                length = len(game)-goal[l]
            else:
                length = goal[l+1]-d
            for r in range(length):
                if game.iloc[d+r, 12]> 0.65:
                    ep.append(d+r)
                    o.append(d)
        j=[game.iloc[ep[1],10]]
        bgb=[ep[1]]
        for r in range(len(ep)-2):
            if o[r]!=o[r+1]:
                ind = ep[r+2]
                j.append(game.iloc[ind,10])
                bgb.append(ind)   
        
                
        g=[goal[0]]          
        a=g[0]
        # between two goals need to be at least 5 seconds
        for r in range (len(goal)-1):
            
            if r < (len(goal)-2):
                if (goal[r+1]-a) > fps*5:
                    if (goal[r+2]-goal[r+1]) <= fps*5:
                        d1 = 1100 - j[r+1]
                        d2= 1100 - j[r+2]
                        if d1 < d2:
                            g.append(goal[r+1])
                            a = goal[r+1]
                        else:
                            g.append(goal[r+2])
                            a=goal[r+2]
                    else:
                        g.append(goal[r+1])
                        a= goal[r+1]
        else:
            if (goal[r+1]-a) > fps*5:
                g.append(goal[r+1])
                a= goal[r+1]
            
    else: 
        g=goal
        

   # file = open(r"C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Foosball/goals/24_fps/"+str(game_id)+'.csv','w') 
   # file.writelines(str(g))
    #file.close()


    return g

g_black = edit(goal, game, 'g11_black')
g_green = edit (goal_g, game, 'g11_green')

