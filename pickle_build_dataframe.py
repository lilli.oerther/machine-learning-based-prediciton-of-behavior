# -*- coding: utf-8 -*-
"""
Created on Sun Aug 20 17:31:43 2023

@author: lilli
"""
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl

data=pd.read_pickle(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\pickle_tournament1\g11DLC_resnet50_foosball_tournamentMay8shuffle1_200000_full.pickle")
d=pd.DataFrame.from_dict(data)

#r=d['frame11259']
#coordinates
#y=pd.DataFrame(x[8])
#a=pd.DataFrame((y[0])[0])
#identities
#y1=pd.DataFrame(x[10])
#a1=pd.DataFrame((y1[0])[0])

coo_=[]
id_=[]

for i in range(0,11760):
    if i<10:
        x=d['frame0000'+str(i)]
    if i>9 and i<100:
        x=d['frame000'+str(i)]
    if i>99 and i<1000:
        x=d['frame00'+str(i)]
    if i>999 and i<10000:
        x=d['frame0'+str(i)]
    if i>9999:
        x=d['frame'+str(i)]
    y=pd.DataFrame(x[8])
    a=pd.DataFrame((y[0])[0])
    coo_.append(a)
    y1=pd.DataFrame(x[10])
    a1=pd.DataFrame((y1[0])[0])
    id_.append(a1)


def sorting (n):
    id_1 =[]
    co_1x=[]
    co_1y=[]
    
    for r in range (len(id_)):
       #for l in range (len(id_[r])):
        a = np.max(id_[r][n])
        if a == 0:
            id_1.append(0)
            j = 0
            co_1x.append(0)
            co_1y.append(0)
        else:
            
            b= id_[r][n]
            c= pd.Index(b).get_loc(a)
            if type(c)!=int:
                if type(c)!=float:
                    id_1.append(0)
                    co_1x.append(0)
                    co_1y.append(0)
                else:
                    id_1.append(a)
                    jx= coo_[r].loc[c,0] 
                    jy= coo_[r].loc[c,1]
                    co_1x.append(jx)
                    co_1y.append(jy)
            else:
                id_1.append(a)
                jx= coo_[r].loc[c,0] 
                jy= coo_[r].loc[c,1]
                co_1x.append(jx)
                co_1y.append(jy)
    
    return id_1, co_1x, co_1y

def sort_2 (id_0, co_0, co_0y):
    co_2x=[]
    co_2y=[] 
    t=[]
    
    #for r in range (len(id_0)):
     #   if (id_0[r] == 1.0):
      #      if  (co_0[r] > 200) :
       #         if (co_0y[r] > 0):
        #            co_2x.append(co_0[r])
         #           co_2y.append(co_0y[r])
          #          t.append(r)
          
    for r in range(len(id_0)):
        t.append(r)
        if (id_0[r]<0.89):
            co_2x.append(np.nan)
            co_2y.append(np.nan)
        elif (co_0[r])< 200:
            co_2x.append(np.nan)
            co_2y.append(np.nan)
        elif (co_0y[r])<=100:
            co_2x.append(np.nan)
            co_2y.append(np.nan)
        else:
            co_2x.append(co_0[r])
            co_2y.append(co_0y[r])
        
    
    return (co_2x, co_2y, t)
            

id_0, co_0, co_0y = sorting(0)
co_0, co_0y, t0 = sort_2(id_0, co_0, co_0y)
id_1, co_1, co_1y = sorting (1)
co_1, co_1y, t1 = sort_2(id_1, co_1, co_1y)
id_2, co_2, co_2y = sorting (2)
co_2, co_2y, t2 = sort_2(id_2, co_2, co_2y)
id_3, co_3, co_3y = sorting (3)
co_3, co_3y, t3 = sort_2(id_3, co_3, co_3y)
id_4, co_4, co_4y = sorting (4)
co_4, co_4y,t4 = sort_2(id_4, co_4, co_4y)
id_5, co_5, co_5y = sorting (5)
co_5, co_5y, t5 = sort_2(id_5, co_5, co_5y)
id_6, co_6, co_6y =sorting (6)
co_6, co_6y, t6 = sort_2(id_6, co_6, co_6y)
id_7, co_7, co_7y = sorting (7)
co_7, co_7y, t7 = sort_2(id_7, co_7, co_7y)
id_8, co_8, co_8y = sorting (8)
co_8, co_8y, t8 = sort_2(id_8, co_8, co_8y)
    
da= [co_0, co_0y, co_1, co_1y, co_2, co_2y, co_3, co_3y, co_4, co_4y, co_5, co_5y, co_6, co_6y, co_7, co_7y, co_8, co_8y]
pickle_data= pd.DataFrame(data=da, index=["row_1_x", "row_1_y", "row_2_x", "row_2_y", "row_3_x", "row_3_y", "row_4_x", "row_4_y", "row_5_x", "row_5_y", "row_6_x", "row_6_y", "row_7_x", "row_7_y","row_8_x", "row_8_y", "ball_x", "ball_y"])
pickle_data= pickle_data.transpose()
pickle_data.to_csv(r'C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\pickle_tournament1\g11DLC_pickle.csv')

plt.figure()

plt.plot(pickle_data['ball_x'], pickle_data['ball_y'], color='black', linewidth=0.1)
plt.plot(pickle_data['row_1_x'], pickle_data['row_1_y'], color='blue', linewidth=0.1)
plt.plot(pickle_data['row_2_x'], pickle_data['row_2_y'], color='red', linewidth=0.1)
plt.title( 'Trajectory Game 11')
plt.xlabel('X-coordinates')
plt.ylabel('Y-coordinates')