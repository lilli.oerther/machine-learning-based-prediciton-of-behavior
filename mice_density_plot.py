# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 13:59:44 2023

@author: lilli
"""

import cv2
import pandas as pd
import numpy as np
from glob import glob
import re
import time
import matplotlib.pylab as plt

import matplotlib as mpl
import seaborn as sns

"""
"""
# functions to load data
# load data participant
def load_participant (x):
    path=pd.DataFrame(glob("C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Hallway Illusion/data/behavior_data/sorted_2/"+str(x)+"/*"))
    path.columns=['location']
    
    d={}
    for i in range (len(path['location'])):
        da=pd.read_csv(str(path['location'].iloc[i]))
        
        p=time.strptime(str(da['date'][0]), '%Y_%b_%d_%H%M' )
        dr=[]
        for r in range (len(da)):
            dr.append(np.cumsum(da['touch_delay']))
        da=da.join(pd.DataFrame({'frames_total': dr}))
        
        d[str(x)+'_0'+str(p[1])+str(p[2])+'_'+str(p[3])+str(p[4])]=da
        
    keys = [k for k, v in d.items()]
        
    return d, keys

def load_track (x):

    path=pd.DataFrame(glob("C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Hallway Illusion/data/tracked_results_DLC/mice_sorted2/"+str(x)+"/*"))
    path.columns=['location']
    
    p=path
    p.loc[:]=p.loc[:].astype(str)

    r=p['location'][0]
    f=r.split('_')

    c=[]
    for i in range (len(p['location'])):
        a=p['location'][i]
        f=re.split('_|2023|(\d+)',a)
        c.append(f[15])
        
    d={}
    for i in range (len(path['location'])):
        da=pd.read_csv(str(path['location'].iloc[i]))
        d[str(x)+'_'+str(c[i])]=da
    
    keys = [k for k, v in d.items()]

    return d, keys

# allignment behavior, DLC data

def touch_point_delay (t_script, mouse_ID, pos):
    d_mouse, keys_behavior = load_participant(mouse_ID)
    track_mouse, keys_track = load_track (mouse_ID)
    
    # load video to get framerate    
    path=pd.DataFrame(glob(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Hallway Illusion\data\maus_videos\mixed\*"))
    path.columns=['location']
    cap=cv2.VideoCapture(str(path['location'].iloc[pos]))
    fps= float(cap.get(cv2.CAP_PROP_FPS)) #get frame rate of original video
    
    touch=d_mouse[keys_behavior[0]]
    touch_delay=touch['touch_delay']
    lick_delay=touch['lick_delay']
    
    touch_points=[]
    
    for r in range (len(touch_delay)): 
        if r==0:
            a=0
        else:
            a= np.sum(lick_delay[0:r])
            
        b = touch_delay[r]
        touch_points.append(a+b)
        
        touch_track = np.array(touch_points) + t_script
        touch_t= touch_track*fps
    
    return touch_delay
    
# load data    
# allignment behavior, DLC data
#delay

def load_delay(r):
    path=pd.DataFrame(glob(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Hallway Illusion\data\merged_behavior_tracked\*"))
    path.columns=['location']
    x_id= str((path.iloc[r])[0])
    
    seq= pd.read_csv(str(x_id))
    seq= seq.columns
    seq= seq[1:-1].astype('float')[0]
    #fps = seq[2]
    
    return seq #,fps


delay = load_delay(1)
delay_2 = load_delay(3)
delay_3 = load_delay(5)
delay_4 = load_delay(7)
delay_5 = load_delay(9)
delay_6 = load_delay(11)
delay_7 = load_delay(13)
delay_8 = load_delay(14)
    
lick_delay = touch_point_delay( delay, '2692', 0)
lick_delay_2 = touch_point_delay( delay_2, '2693', 1)
lick_delay_3 = touch_point_delay( delay_3, '2694', 2)
lick_delay_4 = touch_point_delay( delay_4, '2695', 3)
lick_delay_5 = touch_point_delay( delay_5, '2696', 4)
lick_delay_6 = touch_point_delay( delay_6, '2697', 5)
lick_delay_7 = touch_point_delay( delay_7, '2698', 6)
lick_delay_8 = touch_point_delay( delay_8, '2699', 7)

combine = np.concatenate((lick_delay, lick_delay_2, lick_delay_3, lick_delay_4, lick_delay_5, lick_delay_6, lick_delay_7, lick_delay_8))


plt.figure()
#hist,edges=np.histogram(combine)
distp=sns.distplot(combine, color='black')
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font

x,y = distp.lines[-1].get_data()
xmax = x[np.where(y == y.max())]
outer_xlim = x[np.where((np.cumsum(y)) < 0.9*(np.sum(y)))]
outer_xlim = outer_xlim[-1]

plt.xlim(0,int(outer_xlim+1))

plt.xlabel('Density')
plt.xlabel('Time [s]')
sns.despine()
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Hallway\density.png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Hallway\density.svg", dpi=300,  bbox_inches='tight')
