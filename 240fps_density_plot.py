# -*- coding: utf-8 -*-
"""
Created on Sat Sep 16 02:18:33 2023

@author: lilli
"""


import pandas as pd
import numpy as np
import matplotlib.pylab as plt

import matplotlib as mpl
import seaborn as sns

fps = 240

# function to get float values from goal cvs 
def get_goals_float (goals):
    g = goals.columns
    if len(g) > 1:
        
        g_1= [float(g[0].rsplit('[',1)[-1])]
        g_2 = [float(g[-1].rsplit(']',1)[0])]
        goals= g[1:-1].astype('float').tolist()
        goals = np.concatenate((g_1, goals, g_2))
    else:
        g = g[0].rsplit('[',1)[1]
        g = g.rsplit(']',1)[0]
        if g != '':
            goals = [float(g)]
        else:
            goals = []
    return goals

# load goals
def load_goals (id):
    goals_left = pd.read_csv(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\goals\240_fps\practical_course_raw/"+str(id)+"team_left.csv")
    goals_right = pd.read_csv(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\goals\240_fps\practical_course_raw/"+str(id)+"team_right.csv")
    
    goals_left = get_goals_float (goals_left)
    goals_right = get_goals_float(goals_right)
    
    return goals_left, goals_right
title_list=[24,25,26,28,29,30,31,33,34,35,36,37,38,39,40,41,42,43,44]

combine_f=[]
for r in range(10):#11 games with 24 fps
    ii = title_list[r]
    g_b, g_g = load_goals(ii)
    d = np.sort(np.concatenate((g_b, g_g)))
    v = [d[0]]
    #v=[]
    for k in range(len(d)-1):
        a = d[k+1]-d[k]
        v.append(a)
    combine_f = np.concatenate((combine_f, v))
    
combine = combine_f/fps

plt.figure()

distp=sns.distplot(combine,color='black')
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font

x,y = distp.lines[-1].get_data()
xmax = x[np.where(y == y.max())]
outer_xlim = x[np.where((np.cumsum(y)) < 0.9*(np.sum(y)))]
outer_xlim = outer_xlim[-1]

plt.xlim(0,int(outer_xlim+1))

plt.xlabel('Density')
plt.xlabel('Time [s]')
sns.despine()
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\density.png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\240fps\density.svg", dpi=300,  bbox_inches='tight')

