# -*- coding: utf-8 -*-
"""
Created on Mon Sep  4 13:14:35 2023

@author: lilli
"""

import pandas as pd
import numpy as np

# fuction to load game 
def load_data(game_nr):
    data= pd.read_csv(r"C:\Users\lilli\Desktop\foosball_praktikum\GX0100"+str(game_nr)+"-convertedDLC_resnet50_Kicker TrackingSep13shuffle1_150000.csv")
    data.loc[2:]=data.loc[2:].astype(float)
    
    return data

game_id = 44
    
game= load_data(game_id)
like = game.iloc[2:, 27]
ball_x = game.iloc[2:,25]
ball_y = game.iloc[2:,26]

x_left = 250
x_right = 1750
fps= 240.00
min_f = int(fps)
n1= int(0.25*fps)
n2= int(fps)
pl = 0.9

e=[]
#black
goal=[]
for r in range (len(game)-3):
    if game.iloc[2+r,25]< x_left and game.iloc[2+r,27] > pl and game.iloc[3+r,27]< pl:
        e.append(r)
        if sum (game.iloc[r+2-n1: r+2, 27]) > n1*0.4 and sum(game.iloc[r+2: r+2+n2, 27]) < n2*0.4:
            goal.append(r+2)
            
if len(goal)>0:         
    g=[goal[0]]          
    a=g[0]
    # between two goals need to be at least 5 seconds
    for r in range (len(goal)-1):
        if (goal[r+1]-a) > fps*5:
            g.append(goal[r+1])
            a= goal[r+1]
else: 
    g=goal
    
#green
goal_g=[]
e_g=[]
for r in range (len(game)-3):
    if game.iloc[2+r,25]> x_right and game.iloc[2+r,27] > pl and game.iloc[3+r,27]< pl:
        e_g.append(r)
        if sum (game.iloc[r+2-n1: r+2, 27]) > n1*0.4 and sum(game.iloc[r+2: r+2+n2, 27]) < n2*0.4:
            goal_g.append(r+2)
            
if len(goal_g)>0:         
    g_g=[goal_g[0]]   
    a=g_g[0]       
    # between two goals need to be at least 5 seconds
    for r in range (len(goal_g)-1):
        if (goal_g[r+1]- a) > fps*5:
            g_g.append(goal_g[r+1])
            a= goal_g[r+1]
else: 
    g_g=goal_g
    
    
# decide which frame:
# get frame when ball visible again (sequence > 0.65)
# get frame closest to center

     
#edit   
def edit (goal, game):
    if len(goal)>1:
        ep=[]
        o=[]
        j=[]
        
        for l in range (len(goal)):
            d = goal[l]
            if l == len(goal)-1:
                length = len(game)-goal[l]
            else:
                length = goal[l+1]-d
            for r in range(length):
                if game.iloc[d+r, 27]> 0.65:
                    ep.append(d+r)
                    o.append(d)
        j=[game.iloc[ep[1],25]]
        bgb=[ep[1]]
        for r in range(len(ep)-2):
            if o[r]!=o[r+1]:
                ind = ep[r+2]
                j.append(game.iloc[ind,25])
                bgb.append(ind)   
        
                
        g=[goal[0]]          
        a=g[0]
        # between two goals need to be at least 5 seconds
        for r in range (len(goal)-1):
            
            if r < (len(goal)-2):
                if (goal[r+1]-a) > fps*5:
                    if (goal[r+2]-goal[r+1]) <= fps*5:
                        d1 = 1000 - j[r+1]
                        d2= 1000 - j[r+2]
                        if d1 < d2:
                            g.append(goal[r+1])
                            a = goal[r+1]
                        else:
                            g.append(goal[r+2])
                            a=goal[r+2]
                    else:
                        g.append(goal[r+1])
                        a= goal[r+1]
        else:
            if (goal[r+1]-a) > fps*5:
                g.append(goal[r+1])
                a= goal[r+1]
            
    else: 
        g=goal
            
    return g

g_black = edit(g,game)
g_green = edit(goal_g, game)

def low_like_seq (goals, x):

    goo = []
    for r in range (len(goals)):
        a = goals[r]
        if (goals[r]+5*fps) < len(game):
            z = x[a:int(a+5*fps)]
        else:
            z = x[a:]
        
        for m in range(len(z) - min_f):
            empty = []
            for k in range (min_f):
                if z.iloc[m]<0.65 and z.iloc[m+k]<0.65:
                    empty.append(1)
            if np.sum(empty) == min_f:
                goo.append(a)
    if len(goo)>0:
        goo2 = [goo[0]]
        for r in range (len(goo)-1):
            if goo[r]!=goo[r+1]:
                goo2.append(goo[r+1])
    else:
        goo2=[]
    
    return goo2

green_g_seq = low_like_seq(g_green, like)
black_g_seq = low_like_seq(g_black, like)
        







file = open(r"C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Foosball/goals/240_fps/practical_course/"+str(game_id)+'team_left.csv','w') 
file.writelines(str(black_g_seq))
file.close()

file = open(r"C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Foosball/goals/240_fps/practical_course/"+str(game_id)+'team_right.csv','w') 
file.writelines(str(green_g_seq))
file.close()


